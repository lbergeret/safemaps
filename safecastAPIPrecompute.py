#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Copyright (C) 2011  Lionel Bergeret
#
# ----------------------------------------------------------------
# The contents of this file are distributed under the CC0 license.
# See http://creativecommons.org/publicdomain/zero/1.0/
# ----------------------------------------------------------------

import os
import time
import cPickle
from optparse import OptionParser
import numpy as np
from matplotlib.mlab import griddata
from shapely.geometry import Point
from shapely.ops import cascaded_union
import safecastCommon
import csv
import math

try:
   import psyco
   psyco.full()
except ImportError:
   print "Psyco plugin missing, will run slower"
   pass

# Japan limits
lat_min = 32.0
lon_min = 130.0
lat_max = 46.00
lon_max = 147.45

#coverage['jp:Hokkaido'] = new Array(+42.0, +46.0, +140.0, +146.0);
#coverage['jp:North']    = new Array(+34.0, +42.0, +137.0, +143.0);
#coverage['jp:South']    = new Array(+32.0, +36.0, +130.0, +137.0);

# -----------------------------------------------------------------------------
# Pre-compute the griddata interpolation
# -----------------------------------------------------------------------------
def PreCompute(safecastDataset, safecastDatasetCPMThreashold, safecastGridsize, highResolution):
    # Setup: loading data...
    print "Loading data ..."
    t1 = time.clock()
    nx, ny = safecastGridsize, safecastGridsize # grid size
    npts, x, y, z, zraw, zcount = LoadSafecastData(safecastDataset, safecastDatasetCPMThreashold, lat_min, lat_max, lon_min, lon_max, highResolution)
    print "done in",time.clock()-t1,'seconds.'

    if highResolution:
      xil, yil, grid, missing = ([], [], [], [])
    else:
      # Compute area with missing data
      print "Compute area with missing data"
      t1 = time.clock()
      measures = np.vstack((x,y)).T
      points = [Point(a,b) for a, b in measures]
      spots = [p.buffer(0.04) for p in points] # 0.04 degree ~ 1km radius
      # Perform a cascaded union of the polygon spots, dissolving them into a 
      # collection of polygon patches
      missing = cascaded_union(spots)
      print "done in",time.clock()-t1,'seconds.'

      # Create the grid
      print "Create the grid"
      t1 = time.clock()
      xil = np.linspace(x.min(), x.max(), nx)
      yil = np.linspace(y.min(), y.max(), ny)
      xi, yi = np.meshgrid(xil, yil)
      print "done in",time.clock()-t1,'seconds.'

      # Calculate the griddata
      print "Calculate the griddata (%d x %d)" % (nx, ny)
      t1 = time.clock()
      zi = griddata(x,y,z,xi,yi,interp='nn')
      grid = zi.reshape((ny, nx))
      print "done in",time.clock()-t1,'seconds.'

    toSave = [npts, x, y, z, zraw, zcount, xil, yil, grid, missing]
    cPickle.dump(toSave,open('safecast.pickle','wb'),-1)
    print "Griddata saved (safecast.pickle)."

def dd2dms(dd):
   is_positive = dd >= 0
   dd = abs(dd)
   minutes,seconds = divmod(dd*3600,60)
   degrees,minutes = divmod(minutes,60)
   degrees = degrees if is_positive else -degrees
   return (degrees,minutes,seconds)

def dms2dd(degrees, minutes, seconds):
   return degrees+minutes/60.+seconds/3600.

def truncate(lat, lon, factor):
   # 1 minutes latitude is 40007.86/360/60 = 1.852
   # 25 meters =  0.01349892008639
   # 100 meters = 0.05399568034557235400
   # 500 meters = 0.26997840172786177000

   degrees, minutes, seconds = dd2dms(lat)
   minutes = minutes+(seconds/60)
   minutes -= minutes % round(factor, 4)

   # new latitude
   newLat = degrees+minutes/60

   t = newLat/180*math.pi
   lon_trunc = round(((factor/math.cos(t))), 4)

   degrees, minutes, seconds = dd2dms(lon)
   minutes = minutes+(seconds/60)
   minutes -= minutes % lon_trunc

   # new longitude
   newLon = degrees+minutes/60

   return newLat, newLon

# -----------------------------------------------------------------------------
# Load the Safecast csv data file
# -----------------------------------------------------------------------------
def LoadSafecastData(filename, CPMclip, latmin, latmax, lonmin, lonmax, highResolution):
    # Load data
    data = csv.reader(open(filename))

    # Read the column names from the first line of the file
    fields = data.next()

    safecast = {}    

    # Process data
    for row in data:
      # Zip together the field names and values
      items = zip(fields, row)

      # Add the value to our dictionary
      item = {}
      for (name, value) in items:
         item[name] = value.strip()

      try:
      #if 1:
        # Ignore if outside limits
        if not ((float(item["Latitude"])>latmin) and (float(item["Latitude"])<latmax) and (float(item["Longitude"])>lonmin) and (float(item["Longitude"])<lonmax)):
          continue

        cpm = float(item["Value"])
        cpmRaw = cpm
      
        if (cpm>CPMclip): cpm=CPMclip # clip

        if highResolution:
           lat, lon = truncate(float(item["Latitude"]), float(item["Longitude"]), 0.005399568034557235400) #0.01349892008639) # 25m
        else:
           lat, lon = truncate(float(item["Latitude"]), float(item["Longitude"]), 0.26997840172786177000) # 500m

        key = "%.3f,%.3f" % (lat,lon)
        if key not in safecast:
           safecast[key]=([float(cpm)], [float(cpmRaw)])
        else:
           safecast[key][0].append(float(cpm))
           safecast[key][1].append(float(cpmRaw))
      except:
        pass

    npts = len(safecast)
    print "%s measurements loaded." % npts

    x = []
    y = []
    z = []
    zraw = []
    zcount = []

    # Repack
    for coord in safecast.keys():
      lat, lon = coord.split(",")
      cpm = sum(safecast[coord][0])/len(safecast[coord][0])
      cpmRaw = sum(safecast[coord][1])/len(safecast[coord][1])
      #print lat, lon, cpm, cpmRaw
      x.append(float(lon))
      y.append(float(lat))
      z.append(cpm)
      zraw.append(cpmRaw)
      zcount.append(len(safecast[coord][0]))

    x = np.array(x)
    y = np.array(y)
    z = np.array(z)
    zraw = np.array(zraw)
    zcount = np.array(zcount)
    
    return npts, x, y, z, zraw, zcount

# -----------------------------------------------------------------------------
# Main
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    parser = OptionParser("Usage: safecastPrecompute [options] <safecast-csv-file>")
    parser.add_option("-c", "--clip",
                      type="string", dest="clip", default="350",
                      help="specify the clipping value in CPM")
    parser.add_option("-g", "--gridsize",
                      type="string", dest="gridsize", default="1500",
                      help="specify the grid size for interpolation")
    parser.add_option("-p", "--precision",
                      action="store_true", dest="precision", default=False,
                      help="high resolution truncation 100m square instead of 500m")

    (options, args) = parser.parse_args()
    
    if len(args) != 1:
        parser.error("Wrong number of arguments")

    PreCompute(args[0], int(options.clip), int(options.gridsize), options.precision)
