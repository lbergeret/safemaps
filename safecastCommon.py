#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Copyright (C) 2011  Lionel Bergeret
#
# ----------------------------------------------------------------
# The contents of this file are distributed under the CC0 license.
# See http://creativecommons.org/publicdomain/zero/1.0/
# ----------------------------------------------------------------

# mathematical libraries
import numpy as np
from scipy import interpolate

# matplotlib
from matplotlib import cm, colors
import matplotlib.pyplot as plt

# Math
from math import pi,cos,sin,log,exp,atan
DEG_TO_RAD = pi/180
RAD_TO_DEG = 180/pi

# -----------------------------------------------------------------------------
# Hayakawa-san colormap
# -----------------------------------------------------------------------------
def hayakawasan_cmap():
    levels = [44, 88, 175, 350, 700, 1400, 2800]
    cmap = colors.ListedColormap(['#e0efda', '#c8df97', '#fffa82', '#fccf4e', '#f0a02f', '#ec6828', '#db3036'])
    norm = colors.BoundaryNorm(levels, cmap.N)

    return levels, cmap, norm

# -----------------------------------------------------------------------------
# Discretize a colormap
# -----------------------------------------------------------------------------
def cmap_discretize(cmap, N, crop = 0):
    """Return a discrete colormap from the continuous colormap cmap"""

    cdict = cmap._segmentdata.copy()
    # N colors
    colors_i = np.linspace(crop,1.,N)
    # N+1 indices
    indices = np.linspace(0,1.,N+1)
    for key in ('red','green','blue'):
        # Find the N colors
        D = np.array(cdict[key])
        I = interpolate.interp1d(D[:,0], D[:,1])
        acolors = I(colors_i)
        # Place these colors at the correct indices.
        A = np.zeros((N+1,3), float)
        A[:,0] = indices
        A[1:,1] = acolors
        A[:-1,2] = acolors
        # Create a tuple for the dictionary.
        L = []
        for l in A:
            L.append(tuple(l))
        cdict[key] = tuple(L)

    # Add dark gray at the end
    for key in ('red','green','blue'):
      L = list(cdict[key])
      L[len(L)-1] = (1.0,0.3,0.3) # gray
      cdict[key] = tuple(L)

    return colors.LinearSegmentedColormap('colormap',cdict,1024)

def make_colormap(seq):
    """Return a LinearSegmentedColormap
    seq: a sequence of floats and RGB-tuples. The floats should be increasing
    and in the interval (0,1).
    """
    seq = [(None,) * 3, 0.0] + list(seq) + [1.0, (None,) * 3]
    cdict = {'red': [], 'green': [], 'blue': []}
    for i, item in enumerate(seq):
        if isinstance(item, float):
            r1, g1, b1 = seq[i - 1]
            r2, g2, b2 = seq[i + 1]
            cdict['red'].append([item, r1, r2])
            cdict['green'].append([item, g1, g2])
            cdict['blue'].append([item, b1, b2])
    return colors.LinearSegmentedColormap('CustomMap', cdict)

# -----------------------------------------------------------------------------
# Perform a google projection
# -----------------------------------------------------------------------------
# from http://svn.openstreetmap.org/applications/rendering/mapnik/generate_tiles.py
#
class GoogleProjection:
    def __init__(self,levels=18):
        self.Bc = []
        self.Cc = []
        self.zc = []
        self.Ac = []
        c = 256
        for d in range(0,levels):
            e = c/2;
            self.Bc.append(c/360.0)
            self.Cc.append(c/(2 * pi))
            self.zc.append((e,e))
            self.Ac.append(c)
            c *= 2

    def minmax (self,a,b,c):
        a = max(a,b)
        a = min(a,c)
        return a
                
    def fromLLtoPixel(self,ll,zoom):
         d = self.zc[zoom]
         e = round(d[0] + ll[0] * self.Bc[zoom])
         f = self.minmax(sin(DEG_TO_RAD * ll[1]),-0.9999,0.9999)
         g = round(d[1] + 0.5*log((1+f)/(1-f))*-self.Cc[zoom])
         return (e,g)
     
    def fromPixelToLL(self,px,zoom):
         e = self.zc[zoom]
         f = (px[0] - e[0])/self.Bc[zoom]
         g = (px[1] - e[1])/-self.Cc[zoom]
         h = RAD_TO_DEG * ( 2 * atan(exp(g)) - 0.5 * pi)
         return (f,h)

    def convert(self, gx, gy, zoom):
        # Calculate pixel positions of bottom-left & top-right
        p0 = (gx * 256, (gy + 1) * 256)
        p1 = ((gx + 1) * 256, gy * 256)

        # Convert to LatLong (EPSG:4326)
        l0 = self.fromPixelToLL(p0, zoom);
        l1 = self.fromPixelToLL(p1, zoom);

        # Get tile width and height in degrees
        lonWidth = (l1[0]-l0[0])
        latHeight = (l1[1]-l0[1])

        # Convert main tile position to LatLong (EPSG:4326)
        latlon = self.fromPixelToLL(((gx)*256, (gy+1)*256), zoom); # top-left

        return (latlon[0], lonWidth, latlon[1], latHeight)

# -----------------------------------------------------------------------------
# Mask outside polygons
# -----------------------------------------------------------------------------
# Original from http://stackoverflow.com/questions/3320311/fill-outside-of-polygon-mask-array-where-indicies-are-beyond-a-circular-boundar
# Modified to add multiple polygons support (Lionel)
#
def mask_outside_polygons(polygons, pcolor, ax=None):
    """
    Plots a mask on the specified axis ("ax", defaults to plt.gca()) such that
    all areas outside of the polygon specified by "poly_verts" are masked.  

    "poly_verts" must be a list of tuples of the verticies in the polygon in
    counter-clockwise order.

    Returns the matplotlib.patches.PathPatch instance plotted on the figure.
    """
    import matplotlib.patches as mpatches
    import matplotlib.path as mpath

    if ax is None:
        ax = plt.gca()

    # Get current plot limits
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()

    # Verticies of the plot boundaries in clockwise order
    bound_verts = [(xlim[0], ylim[0]), (xlim[0], ylim[1]), 
                   (xlim[1], ylim[1]), (xlim[1], ylim[0]), 
                   (xlim[0], ylim[0])]

    # A series of codes (1 and 2) to tell matplotlib whether to draw a line or 
    # move the "pen" (So that there's no connecting line)
    bound_codes = [mpath.Path.MOVETO] + (len(bound_verts) - 1) * [mpath.Path.LINETO]
    poly_codes = []
    poly_verts = []
    for poly in polygons:
      poly_codes += [mpath.Path.MOVETO] + (len(poly) - 1) * [mpath.Path.LINETO]
      poly_verts += poly

    # Plot the masking patch
    path = mpath.Path(bound_verts + poly_verts, bound_codes + poly_codes)
    patch = mpatches.PathPatch(path, facecolor=pcolor, edgecolor='none')
    patch = ax.add_patch(patch)

    # Reset the plot limits to their original extents
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)

    return patch

# -----------------------------------------------------------------------------
# Make a color transparent from an image
# -----------------------------------------------------------------------------
def makeColorTransparent(image, color):
    """
    Replace the color from the input image by transparent
    """
    img = image
    img = img.convert("RGBA")
    pixdata = img.load()

    for y in xrange(img.size[1]):
       for x in xrange(img.size[0]):
          if pixdata[x, y] == color:
              pixdata[x, y] = (255, 255, 255, 0)

    return img

# -----------------------------------------------------------------------------
# AABB intersections
# -----------------------------------------------------------------------------
# from http://www.panda3d.org/forums/viewtopic.php?t=5817
class AABB2D():
    '''Axis-aligned bounding box for 2D shapes.

    Represents minimum and maximum X and Y coordinates for the contained
    shape.'''

    def __init__(self, points):
        '''To be constructed, AABB requires a list of points with two
        coordinates (X, Y) each. If present, Z coordinate is ignored.'''

        self.minX = self.maxX = points[0][0]
        self.minY = self.maxY = points[0][1]

        self._calc(points)

    def _calc(self, points):
        i = 1 # Not from 0!
        while i < len(points):
            px, py = points[i]
            self.minX = min(self.minX, px)
            self.maxX = max(self.maxX, px)
            self.minY = min(self.minY, py)
            self.maxY = max(self.maxY, py)
            i += 1

    def getCenter(self):
        '''Returns the center of the AABB.'''

        x = (self.minX + self.maxX) / 2.0
        y = (self.minY + self.maxY) / 2.0
        return (x, y)

    def isInside(self, point):
        '''Returns True if the given point is inside of this AABB.'''

        px, py = point
        if (px - self.minX) * (px - self.maxX) < 0:
            return False
        if (py - self.minY) * (py - self.maxY) < 0:
            return False
        return True

    def intersect(self, aabb):
        '''Tests if this AABB intersects with another AABB.
       
        Returns a tuple of values that describe the area of intersection:
        (leftmost X of intersection, rightmost X of itersection,
        lowest Y of intersection, highest Y of itersection),
        or (min X, max X, min Y, max Y).
        Or 'False' if they don't intersect.'''

        if self.minX > aabb.maxX or self.maxX < aabb.minX:
            return False
        if self.minY > aabb.maxY or self.maxY < aabb.minY:
            return False
        minX = max(self.minX, aabb.minX)
        maxX = min(self.maxX, aabb.maxX)
        minY = max(self.minY, aabb.minY)
        maxY = min(self.maxY, aabb.maxY)
        return (minX, maxX, minY, maxY) 

