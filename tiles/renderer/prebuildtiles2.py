#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Copyright (C) 2011  Lionel Bergeret
#
# ----------------------------------------------------------------
# The contents of this file are distributed under the CC0 license.
# See http://creativecommons.org/publicdomain/zero/1.0/
# ----------------------------------------------------------------

import cgi, cgitb

import cPickle
from optparse import OptionParser
import os
import sys
import time
sys.path.append(os.path.dirname(os.path.abspath(__file__))+ "/../..")

# Download URL
import urllib

# Multiprocess
from multiprocessing import Pool

# Matplotlib
import matplotlib
matplotlib.use('Agg') # for CGI script (no display)
import matplotlib.pyplot as plt
from matplotlib import cm, colors
from matplotlib.mlab import griddata
from mpl_toolkits.basemap import Basemap as Basemap
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.nxutils import points_inside_poly

# Scipy, Numpy and pylab
from scipy import interpolate
import numpy as np

# Shapely
from shapely.geometry import Polygon, MultiPolygon
from shapely.ops import cascaded_union

# PIL
try:
  from PIL import Image, ImageMath
except:
  import Image, ImageMath

# Safecast common
from safecastCommon import GoogleProjection, cmap_discretize, mask_outside_polygons, makeColorTransparent, hayakawasan_cmap, make_colormap

# Japan limits
jp_lat_min = 31.0
jp_lon_min = 130.0
jp_lat_max = 46.00
jp_lon_max = 147.45

#coverage['jp:Hokkaido'] = new Array(+42.0, +46.0, +140.0, +146.0);
#coverage['jp:North']    = new Array(+34.0, +42.0, +137.0, +143.0);
#coverage['jp:South']    = new Array(+32.0, +36.0, +130.0, +137.0);

# Default parameter
gridSize = 4
concurrency = 6
rendererFolder = "./"
tileFolder = "cached"
shapefile = "../../data/JPN_adm1"

uncovered = False # generate uncovered yellow areas only

googleWaterColorHtml = "#a5bfdd"
googleWaterColor = (165, 191, 221, 255)

def DrawTile(precision, lon_min, lon_width, lat_min, lat_height, gx, gy, gzoom, gsize_lon, gsize_lat, x, y, z, xi, yi, grid, missing, uncovered, coastline, waterbodies, name, tileSize = 256):
    fig = Figure()
    canvas = FigureCanvas(fig)

    # Precompute gsize x gsize tiles in one shot
    lon_max = lon_min + gsize_lon * lon_width
    lat_max = lat_min + gsize_lat * lat_height

    # Create the basemap and load the shapefile
    m = Basemap(projection='merc', llcrnrlon=lon_min ,llcrnrlat=lat_min, urcrnrlon=lon_max ,urcrnrlat=lat_max, resolution='i')

    # Cleanup all axes, title, ... from the canvas
    dpi = 64.0
    m.ax = fig.add_axes([0, 0, 1, 1], axis_bgcolor=(1.0,1.0,1.0,0.0), frameon=False)
    m.ax.patch.set_linewidth(0.0)
    fig.set_size_inches((tileSize*gsize_lon)/dpi, (tileSize*gsize_lat)/dpi) # we need 256x256 pixel images
    fig.set_facecolor((1.0,1.0,1.0,0.0))
    fig.figurePatch.set_alpha(0.0)
    fig.gca().axesPatch.set_alpha(0.0)

    #japan_shp_info = m.readshapefile("%s/%s" % (rendererFolder, shapefile),"%s/%s" % (rendererFolder, shapefile), color='b', linewidth = 0.5)

    # Compute Safecast color map
    cdict = {'blue': ((0.0, 0.0, 0.58431375026702881), (0.0625, 0.58431375026702881, 0.66535947774750537), (0.125, 0.66535947774750537, 0.74379085138731904), (0.1875, 0.74379085138731904, 0.81960786510915984), (0.25, 0.82352940451865631, 0.68758171430600235), (0.4375, 0.68758171430600235, 0.56470589731253651), (0.625, 0.56470589731253651, 0.44183006045085643), (0.6875, 0.44183006045085643, 0.34117646953639025), (0.75, 0.34117646953639025, 0.26274510890829772), (0.8125, 0.26274510890829772, 0.18954249004912516), (0.875, 0.18954249004912516, 0.15163399275221856), (0.9375, 0.15163399275221856, 0.14901961386203766), (1.0, 0.3, 0.3)),
            'green': ((0.0, 0.0, 0.21176470816135406), (0.0625, 0.21176470816135406, 0.37647055136223395), (0.125, 0.37647055136223395, 0.5320261256367439), (0.1875, 0.5320261256367439, 0.67843141228544335), (0.25, 0.98431373016506907, 0.95947712418300657), (0.4375, 0.95947712418300657, 0.87843136810788924), (0.625, 0.87843136810788924, 0.74771241385952325), (0.6875, 0.74771241385952325, 0.59738561218860187), (0.75, 0.59738561218860187, 0.42745097758723238), (0.8125, 0.42745097758723238, 0.26797385535209006), (0.875, 0.26797385535209006, 0.1254901966627906), (0.9375, 0.1254901966627906, 0.0), (1.0, 0.3, 0.3)),
            'red': ((0.0, 0.0, 0.19215686619281769), (0.0625, 0.19215686619281769, 0.24444444224335923), (0.125, 0.24444444224335923, 0.33202613183095686), (0.1875, 0.33202613183095686, 0.4549019891841698), (0.25, 0.95947713625976172, 0.99869281045751634), (0.4375, 0.99869281045751634, 0.99607843137254903), (0.625, 0.99607843137254903, 0.99346405228758172), (0.6875, 0.99346405228758172, 0.98039215487592413), (0.75, 0.98039215487592413, 0.95686274427993645), (0.8125, 0.95686274427993645, 0.8810457564646903), (0.875, 0.8810457564646903, 0.7777777890753903), (0.9375, 0.7777777890753903, 0.64705884456634521), (1.0, 0.3, 0.3))}

    cmap = colors.LinearSegmentedColormap('colormap',cdict,1024)
    #cmap = cmap_discretize(cm.RdYlBu_r, 16, 0.)
    normCPM = colors.Normalize(vmin=0,vmax=350)
    levels = range(0, 400, 350/16)

    # Hayakawa-san color map
    # levels, cmap, normCPM = hayakawasan_cmap()

    # Exclude uncovered areas
    if uncovered:
      for patch in missing.geoms:
        assert patch.geom_type in ['Polygon']
        assert patch.is_valid

        if patch.area > 0.0016: # more than (0.04 degree x 0.04 degree) ~ (1km x 1km) area
          # Fill and outline each patch
          x, y = patch.exterior.xy
          x, y = m(x, y)
          m.ax.fill(x, y, color='#FFFF00', aa=True, alpha=1.0, hatch="x")
          m.plot(x, y, color=googleWaterColorHtml, aa=True, lw=1.0, alpha=0.0) # needed for basemap to scale/crop the area

    # Draw countour interpolation map
    if not uncovered:
      xim, yim = m(*np.meshgrid(xi, yi))
      m.contourf(xim,yim, grid, levels, cmap=cmap, norm=normCPM)

    # Draw Safecast data on the map
    if not uncovered:
      lon,lat = m(x,y)
      m.scatter(lon,lat,s=2, c=z, cmap=cmap, norm=normCPM, linewidths=0.2, alpha=1.0) # set alpha = 0.1 to disable

    # Clip outside coastlines area and water bodies
    if len(coastline) > 0 and len(waterbodies) > 0:
      polygonsToClip = []
      for patch in coastline.geoms:
        if not patch.is_empty and patch.is_valid:
          vx, vy = patch.exterior.xy
          vx.reverse()
          vy.reverse()
          mvx, mvy = m(vx,vy)
          polygonsToClip.append(zip(mvx, mvy))

      for patch in waterbodies.geoms:
        if not patch.is_empty and patch.is_valid:
          vx, vy = patch.exterior.xy
          mvx, mvy = m(vx,vy)
          polygonsToClip.append(zip(mvx, mvy))

      mask_outside_polygons(polygonsToClip, googleWaterColorHtml, ax = m.ax)

    # Save the result
    if not os.path.exists("%s/%s/%s" % (rendererFolder, tileFolder, gzoom)):
      os.makedirs("%s/%s/%s" % (rendererFolder, tileFolder, gzoom))

    #
    # Multiple tiles
    #
    if (gsize_lon > 1):
      tileNameTemp = "%s/%s/%s/%s" % (rendererFolder, tileFolder, gzoom, name)
      canvas.print_figure(tileNameTemp, dpi=dpi)

      # Start the tile cutting process
      image = Image.open(tileNameTemp)
      tile_width = tileSize
      tile_height = tileSize

      # Cut the tiles
      currentx = 0
      currenty = 0
      googlex = gx
      googley = gy
      while currenty < image.size[1]:
        while currentx < image.size[0]:
          tile = image.crop((currentx,currenty,currentx + tile_width,currenty + tile_height))
          tilename = "safecast_griddata_%s_%s_%s.png" %(googlex, googley, gzoom)
          filename = "%s/%s/%s/%s" % (rendererFolder, tileFolder, gzoom, tilename)

          if not os.path.exists(filename):
            makeColorTransparent(tile, googleWaterColor).save(filename)

          currentx += tile_width
          googlex += 1

        currentx = 0
        currenty += tile_height

        googley += 1
        googlex = gx
      os.remove(tileNameTemp)

    #
    # One tile
    #
    else:
      name = "safecast_griddata_%s_%s_%s.png" %(gx, gy, gzoom)
      tileName = "%s/%s/%s/%s" % (rendererFolder, tileFolder, gzoom, name)
      canvas.print_figure(tileName, dpi=dpi)
      makeColorTransparent(Image.open(tileName), googleWaterColor).save(tileName)
      if tileSize > 256:
        svgname = "safecast_griddata_%s_%s_%s.svg" %(gx, gy, gzoom)
        svgtileName = "%s/%s/%s/%s" % (rendererFolder, tileFolder, gzoom, svgname)
        print "Render SVG"
        canvas.print_figure(svgtileName, dpi=dpi)

    # Clear the plot (free the memory for the other threads)
    plt.clf()

def interpolate(gx, gy, gzoom, gsize, x, y, z, xi, yi, grid, missing, uncovered, projection, coastline, waterbodies):
    print "%s> (%s %s) %s" % (gzoom, gx, gy, gsize)

    # Create the cached filename
    tilename = "safecast_griddata_%s_%s_%s.png" %(gx, gy, gzoom)
    filename = "%s/%s/%s/%s" % (rendererFolder, tileFolder, gzoom, tilename)
    tilearea = projection.convert(gx,gy, gzoom)

    # Draw the new tiles
    DrawTile(60, tilearea[0], tilearea[1], tilearea[2], tilearea[3], gx, gy, gzoom, gsize, 1, x, y, z, xi, yi, grid, missing, uncovered, coastline, waterbodies, "safecast_temp_%s_%s_%s.png" %(gx, gy, gzoom))

def optimizeArea(tilearea, gsize, coastline):
    # Check if tile area is inside Japan coastlines
    inside = 0
    area = [(tilearea[0], tilearea[2]), (tilearea[0]+gsize*tilearea[1], tilearea[2]), (tilearea[0]+gsize*tilearea[1], tilearea[2]+tilearea[3]), (tilearea[0], tilearea[2]+tilearea[3])]

    aminx = 9999
    aminy = 9999
    amaxx = 0
    amaxy = 0

    for patch in coastline.geoms:
      if not patch.is_empty and patch.is_valid:
        vx, vy = patch.exterior.xy
        points = zip(vx, vy)

        # Compute intersections with the city
        intersection = points_inside_poly(points, area)

        for i in range(len(intersection)-1):
          if (intersection[i]):
            aminx = min(aminx, points[i][0])
            aminy = min(aminy, points[i][1])
            amaxx = max(amaxx, points[i][0])
            amaxy = max(amaxy, points[i][1])
            inside += 1

    if inside == 0:
      return False, 0, 0, 0

    # Re-evaluate rendering area
    a = int((aminx-tilearea[0])//tilearea[1])
    b = int((amaxx-tilearea[0])//tilearea[1])
    newMinLon = tilearea[0] + a*tilearea[1]
    newgsize = b-a+1
    if newgsize != gsize:
       #print "New rendering size %d -> %d" % (gsize, newgsize)
       gsize = newgsize
    return True, a, b, gsize

# from http://stackoverflow.com/questions/6728236/exception-thrown-in-multiprocessing-pool-not-detected
# Pool debugging

# -----------------------------------------------------------------------------
# Main
# -----------------------------------------------------------------------------
if __name__ == "__main__":
 parser = OptionParser("Usage: prebuildtiles [options] <safecast-csv-file>")
 parser.add_option("-u", "--uncovered",
                      action="store_true", dest="uncovered", default=False,
                      help="generate uncovered yellow area to the map (default is none)")
 parser.add_option("-p", "--preview",
                      action="store_true", dest="preview", default=False,
                      help="render a quick preview map")
 parser.add_option("-z", "--zoom",
                    type=int, dest="zoom", default=[], action='append',
                    help="render only this zoom level")

 (options, args) = parser.parse_args()

 projection = GoogleProjection()

 # Load precomputed safecast griddata
 print "Loading precomputed safecast griddata"
 [npts, x, y, z, zraw, zcount, xi, yi, grid, missing] = cPickle.load(open('%s/safecast.pickle' % rendererFolder,'rb'))

 # Load precomputed coastline if any
 print "Loading precomputed coastline"
 try:
   coastline = cPickle.load(open("%s/coastline.pickle" % rendererFolder,'rb'))
 except:
   coastline = []
   pass

 # Load precomputed waterbodies if any
 print "Loading precomputed waterbodies"
 try:
   waterbodies = cPickle.load(open("%s/waterbodies.pickle" % rendererFolder,'rb'))
 except:
   waterbodies = []
   pass

 if options.uncovered:
   # All uncovered areas
   print "Computing uncovered areas"
   difference = coastline.difference(missing)
   missing = difference

 if options.preview:
   print "Rendering preview"
   DrawTile(60, jp_lon_min, jp_lon_max-jp_lon_min, jp_lat_min, jp_lat_max-jp_lat_min, 1, 1, 1, 1, 1, x, y, z, xi, yi, grid, missing, options.uncovered, coastline, waterbodies, "safecast.png", 2048)
   sys.exit(0)

 print "Start rendering process ..."
 if options.zoom != []:
   zoomrange = options.zoom
 else:
   zoomrange = range(4, 13)

 print "Zoom range =", zoomrange
 for gzoom in zoomrange:
   print "========== Zoom level", gzoom, "=========="

   gx0 , gy0 = projection.fromLLtoPixel((jp_lon_min, jp_lat_max),gzoom) # top right
   gx1 , gy1 = projection.fromLLtoPixel((jp_lon_max, jp_lat_min),gzoom) # bottom left

   gx0 = int(gx0/256)
   gy0 = int(gy0/256)

   gx1 = int(gx1/256)
   gy1 = int(gy1/256)

   # Optimal size for the slice computation
   gridSize = gx1 - gx0 + 1
   if gridSize == 0: gridSize = 1
   if gridSize > 82: gridSize = 64

   pool = Pool(processes = concurrency)

   # Start looping
   gx = gx0
   gy = gy0
   while (gy <= gy1):
     # Horizontal slice
     tilearea = projection.convert(gx,gy, gzoom)
     gsize = gx1-gx0+1
     # Optimize rendering area
     result, a, b, gsize = optimizeArea(tilearea, gsize, coastline)
     #print result, gx, (gx0, gx1), gy, a, b, gsize

     if result:
       gx = gx0 + a
       maxX = gx0 + b
     else:
       print "%s> (%s %s) %s skipped" % (gzoom, gx, gy, gx1-gx0+1)
       gx = gx0
       gy += 1
       continue

     while (gx <= maxX):
       # Adjust slice horizontal size
       if gridSize > maxX-gx+1:
         gsize = maxX-gx+1
       else:
         gsize = gridSize

       # Create the cached filename
       #interpolate(gx, gy, gzoom, gsize, x, y, z, xi, yi, grid, missing, options.uncovered, projection, coastline, waterbodies)
       pool.apply_async(interpolate, (gx, gy, gzoom, gsize, x, y, z, xi, yi, grid, missing, options.uncovered, projection, coastline, waterbodies))

       gx += gridSize

     gx = gx0
     gy += 1

   # Wait the pool to be completed
   pool.close()
   pool.join()
